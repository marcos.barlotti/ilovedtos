﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BO.Services.Base
{
    internal class ErrorDetails
    {
        [JsonPropertyName("status")]
        public int Status { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("errors")]
        public List<string> Errors { get; set; }
    }
}
