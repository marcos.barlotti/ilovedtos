﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Services.Base
{
    public class BackendException : Exception
    {
        public BackendException(List<string> errors, string error) : base(error)
        {
            Errors = errors;
        }
        public List<string> Errors { get; private set; }
    }
}
