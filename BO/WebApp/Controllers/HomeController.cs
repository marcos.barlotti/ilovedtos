﻿using GeneradorEntidadDTO.Models.DTO;
using GeneradorEntidadDTO.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        #region Dependencias
        private readonly ILogger<HomeController> _logger;
        private readonly IGeneradorService _generadorService;
        #endregion

        #region Constructor
        public HomeController(
             ILogger<HomeController> logger,
              IGeneradorService generadorService)
        {
            _logger = logger;
            _generadorService = generadorService;
        }
        #endregion



        public IActionResult Index()
        {
            ParametrosProfileDTO hola = new ParametrosProfileDTO();

            //hola.ListaColumnas.AddRange(new List<string>() { "ENGE", "C2_1_0_3", "C1_1_2", "C1_1_3", "C1_1_4", "C1_2_2", "C1_2_3", "C1_2_4", "C1_3_2", "C1_3_3", "C1_3_4", "C1_8_2", "C1_8_3", "C1_8_4", "C2_0_0_2", "C2_0_0_3", "C2_0_0_4", "C2_1_1_3", "C2_1_2_3", "C2_1_3_3", "C2_2_0_3", "C2_3_0_3", "C3_1_2", "C3_1_3", "C3_1_4", "C3_2_0_3", "C3_2_1_3", "C3_2_2_3", "C3_2_3_3", "C3_3_2", "C3_3_3", "C3_3_4", "C3_4_2", "C3_4_3", "C3_4_4", "C3_5_2", "C3_5_3", "C3_5_4", "C3_10_2", "C3_10_3", "C3_10_4", "C4_0_0_2", "C4_0_0_4", "C1_4_2", "C1_4_3", "C1_4_4", "C3_6_2", "C3_6_3", "C3_6_4", "ID_EMPRS", "C1_1_1", "C1_2_1", "C1_3_1", "C1_5_0", "C1_5_1", "C1_5_2", "C1_5_3", "C1_5_4", "C1_6_0", "C1_6_1", "C1_6_2", "C1_6_3", "C1_6_4", "C1_7_0", "C1_7_1", "C1_7_2", "C1_7_3", "C1_7_4", "C2_1_1_1", "C2_1_2_1", "C2_1_3_1", "C2_2_0_1", "C3_1_1", "C3_2_1_1", "C3_2_2_1", "C3_2_3_1", "C3_3_1", "C3_4_1", "C3_5_1", "C3_7_0", "C3_7_1", "C3_7_2", "C3_7_3", "C3_7_4", "C3_8_0", "C3_8_1", "C3_8_2", "C3_8_3", "C3_8_4", "C3_9_0", "C3_9_1", "C3_9_2", "C3_9_3", "C3_9_4", "C1_4_1", "C3_6_1", "FECHA", "C2_4_0_1", "C2_4_0_3", "C1_1_5", "C1_2_5", "C1_3_5", "C1_4_5", "C1_5_5", "C1_6_5", "C1_7_5", "C1_8_5", "C2_0_0_5", "C2_1_0_5", "C2_1_1_5", "C2_1_2_5", "C2_1_3_5", "C2_2_0_5", "C2_3_0_5", "C2_4_0_5", "C3_10_5", "C3_2_0_5", "C3_2_1_5", "C3_2_2_5", "C3_2_3_5", "C3_3_5", "C3_4_5", "C3_5_5", "C3_6_5", "C3_7_5", "C3_8_5", "C3_9_5", "C1_8_2A", "C1_1_2A", "C1_2_2A", "C1_3_2A", "C1_4_2A", "C1_5_2A", "C1_6_2A", "C1_7_2A", "C2_0_0_2A", "C2_1_0_2A", "C2_2_0_2A", "C2_3_0_2A", "C2_4_0_2A", "C2_1_1_2A", "C2_1_2_2A", "C2_1_3_2A", "C3_10_2A", "C3_2_0_2A", "C3_2_1_2A", "C3_2_2_2A", "C3_2_3_2A", "C3_3_2A", "C3_4_2A", "C3_5_2A", "C3_6_2A", "C3_7_2A", "C3_8_2A", "C3_9_2A" });

            _generadorService.GeneradorProfile(hola);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}


