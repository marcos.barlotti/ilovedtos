﻿using GeneradorEntidadDTO.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Services.Interfaces
{
    public interface IGeneradorService
    {
        ByteDTO GetArchivo(ParametrosEntidadDTO parametros);
        ByteDTO GeneradorProfile(ParametrosProfileDTO parametros);
    }
}
