﻿using GeneradorEntidadDTO.Models.DTO;
using GeneradorEntidadDTO.Models.Support;
using GeneradorEntidadDTO.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Services.Implements
{
    public class GeneradorService : IGeneradorService
    {
        public GeneradorService()
        {
        }

        public ByteDTO GetArchivo(ParametrosEntidadDTO parametros)
        {
            StringBuilder sbFile = new();
            List<EntidadDTO> listaEntidades = Formatter.OrdenarLista(parametros.ListaColumnas, parametros.ListaDatos, parametros.ListaNulleable);

            Builder.BuildEntidadDto(parametros.NombreClase, parametros.NameSpace ,listaEntidades, sbFile, parametros.EsDto);

            var ms = Formatter.StringBuilderToMemoryStream(sbFile);

            var fileName = $"{parametros.NombreClase}{(parametros.EsDto ? "DTO" : "")}.cs";

            Formatter.AddFilePath(parametros.Directorio, fileName, sbFile);

            return new ByteDTO { fileName = fileName, content = ms , Directorio = parametros.Directorio};
        }

       
        public ByteDTO GeneradorProfile(ParametrosProfileDTO parametros)
        {
            StringBuilder sbFile = new();

            Builder.BuildProfile(parametros, sbFile);

            var ms = Formatter.StringBuilderToMemoryStream(sbFile);
            var fileName = $"{parametros.NombreClase}Profile.cs";
            Formatter.AddFilePath(parametros.Directorio, fileName, sbFile);

            return new ByteDTO { fileName = fileName, content = ms , Directorio = parametros.Directorio};
        }
    }
}
