﻿using GeneradorEntidadDTO.Models.DTO;
using GeneradorEntidadDTO.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Runtime.Serialization;
using System.Text;

namespace GeneradorEntidadDTO.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneradorController : ControllerBase
    {
        #region Dependencias
        private readonly IGeneradorService _generadorService;
        #endregion

        #region Constructor
        public GeneradorController(
            IGeneradorService generadorService
            )
        {
            _generadorService = generadorService;
        }
        #endregion


        [HttpPost]
        [Route("GeneraEntidadDto")]
        public async Task<IActionResult> GeneraEntidadDto(ParametrosEntidadDTO parametros)
        {
            var file = _generadorService.GetArchivo(parametros);
            return File(file.content, "application/octet-stream", file.fileName);
        }

        [HttpPost]
        [Route("GeneraProfile")]
        public async Task<IActionResult> GeneraProfile(ParametrosProfileDTO parametros)
        {
            var file = _generadorService.GeneradorProfile(parametros);
            return File(file.content, "application/octet-stream", file.fileName);
        }
    }
}
