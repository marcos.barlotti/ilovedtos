﻿using GeneradorEntidadDTO.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Models.Support
{
    public static class Formatter
    {
        public static List<EntidadDTO> OrdenarLista(List<string> listaColumnas, List<string> listaDatos, List<string> listaNulleable)
        {

            List<EntidadDTO> listaEntidades = new List<EntidadDTO>();
            for (int i = 0; i < listaColumnas.Count(); i++)
            {
                listaEntidades.Add(new EntidadDTO()
                {
                    Columna = listaColumnas[i],
                    TipoDato = listaDatos[i],
                    Nulleable = listaNulleable[i]
                });
            }

            return listaEntidades;

        }
        public static int StringLengthGenerator(string varchar)
        {
            varchar = varchar.Replace("VARCHAR2(", "");
            varchar = varchar.Replace(" BYTE)", "");
            return Int32.Parse(varchar);
        }

        public static string TranformaTipoDato(string tipoDato)
        {
            tipoDato = tipoDato.Replace(" ", "");
            switch (tipoDato)
            {
                case "NUMBER(16,0)":
                    return "long";
                case "NUMBER(4,0)":
                    return "int";
                case "DATE":
                    return "DateTime";
                default:
                    return "DATO NO ENCONTRADO";
            }
        }

        public static MemoryStream StringBuilderToMemoryStream(StringBuilder sbFile)
        {
            var ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms);
            tw.Write(sbFile.ToString());
            tw.Flush();
            ms.Position = 0;

            return ms;
        }

        public static string AddChar(char param, int cantidad)
        {
            string cadena = new string(param, cantidad);
            return cadena;
        }

        public static void AddFilePath(string fileFolder, string fileName, StringBuilder sbFile)
        {
            var fileByte = Encoding.UTF8.GetBytes(sbFile.ToString());
            
            if (!string.IsNullOrEmpty(fileFolder))
            {
                File.WriteAllBytes(fileFolder + "\\" + fileName, fileByte);
            }
        }


    }
}
