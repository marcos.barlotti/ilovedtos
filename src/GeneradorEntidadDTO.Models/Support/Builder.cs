﻿using GeneradorEntidadDTO.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Models.Support
{
    public static class Builder
    {
        static public void BuildEntidadDto(string nombreClase,string nameSpace ,List<EntidadDTO> listaEntidades, StringBuilder sbFile, bool dto = false)
        {
            var tipoDato = "";
            var stringLength = 0;
            sbFile.AppendLine("using System;");
            sbFile.AppendLine("using System.Collections.Generic;");
            sbFile.AppendLine("using System.ComponentModel.DataAnnotations;");
            sbFile.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");
            sbFile.AppendLine("using System.Linq;");
            sbFile.AppendLine("using System.Text;");
            sbFile.AppendLine("using System.Threading.Tasks;");
            sbFile.AppendLine("");
            sbFile.AppendLine($"namespace {nameSpace}");
            sbFile.AppendLine("{");
            sbFile.AppendLine($"    public class {nombreClase}");
            sbFile.AppendLine("    {");
            foreach (var item in listaEntidades)
            {

                if (item.Nulleable.ToLower() == "no")
                {
                    sbFile.AppendLine("[Required(ErrorMessage = \"El campo {0} es requerido\")]");
                }

                if (item.TipoDato.Contains("VARCHAR"))
                {
                    tipoDato = "string";
                    stringLength = Formatter.StringLengthGenerator(item.TipoDato);
                    sbFile.AppendLine($"[StringLength({stringLength})]");
                    sbFile.AppendLine("[DataType(DataType.Text)]");
                }
                else
                {
                    tipoDato = Formatter.TranformaTipoDato(item.TipoDato);
                }

                if (dto)
                {
                    sbFile.AppendLine($"[Display(Name= \"{item.Columna}\")]");
                }
                else
                {
                    sbFile.AppendLine($"[Column(\"{item.Columna}\")]");
                }
                sbFile.AppendLine($"public {tipoDato} {item.Columna} " + "{get; set;}");
                sbFile.AppendLine("");
            }
            sbFile.AppendLine("}");
            sbFile.AppendLine("}");
        }

        public static void BuildProfile(ParametrosProfileDTO parametros, StringBuilder sbFile)
        {

            string tab = Formatter.AddChar(' ', 4);
            string dobletab = Formatter.AddChar(' ', 4);
            string tripleTab = Formatter.AddChar(' ', 12);
            string cuadrupleTab = Formatter.AddChar(' ', 16);

            sbFile.AppendLine("using AutoMapper;");
            sbFile.AppendLine($"using {parametros.NameSpace}.DTO;");
            sbFile.AppendLine($"using {parametros.NameSpace}.Entities;");
            sbFile.AppendLine("");

            sbFile.AppendLine($"namespace {parametros.NameSpace}");
            sbFile.AppendLine("{");
            sbFile.AppendLine($"{tab}public class {parametros.NombreClase} : Profile");
            sbFile.AppendLine(tab + "{");
            sbFile.AppendLine($"{tab}{tab}public {parametros.NombreClase}Profile()");
            sbFile.AppendLine(dobletab + "{");
            sbFile.AppendLine($"{tripleTab}CreateMap<{parametros.NombreClase}, {parametros.NombreClase}DTO>()");
            foreach (var item in parametros.ListaColumnas)
            {
                sbFile.AppendLine($"{cuadrupleTab}.ForMember(destino => destino.{item}, option => option.MapFrom(origen => origen.{item}))");
            }
            sbFile.AppendLine($"{cuadrupleTab}.ReverseMap();");
            sbFile.AppendLine(dobletab + "}");
            sbFile.AppendLine(tab + "}");
            sbFile.AppendLine("}");
        }

        
    }
}

	       	


   
