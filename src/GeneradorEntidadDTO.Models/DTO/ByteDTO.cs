﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Models.DTO
{
    public class ByteDTO
    {
        public string fileName { get; set; }
        public MemoryStream content { get; set; }
        public string Directorio { get; set; }
    }
}
