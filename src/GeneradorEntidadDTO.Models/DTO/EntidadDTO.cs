﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Models.DTO
{
    public class EntidadDTO
    {
        public string Columna { get; set; }
        public string TipoDato { get; set; }
        public string Nulleable { get; set; }

    }
}
