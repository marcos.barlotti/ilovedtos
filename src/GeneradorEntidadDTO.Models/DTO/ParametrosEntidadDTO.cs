﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorEntidadDTO.Models.DTO
{
    public class ParametrosEntidadDTO
    {
        public bool EsDto { get; set; }
        public string NombreClase { get; set; }
        public string NameSpace{ get; set; }
        public string Directorio { get; set; }
        public List<string> ListaColumnas { get; set; } = new List<string>();
        public List<string> ListaDatos { get; set; } = new List<string>();
        public List<string> ListaNulleable { get; set; } = new List<string>();
       
    }
}
